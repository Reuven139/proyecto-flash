package com.salesianos.triana.inmobiliariaflash;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.salesianos.triana.inmobiliariaflash.model.MyPropertyResponse;
import com.salesianos.triana.inmobiliariaflash.model.PropertyResponse;
import com.salesianos.triana.inmobiliariaflash.model.ResponseContainer;
import com.salesianos.triana.inmobiliariaflash.model.UtilToken;
import com.salesianos.triana.inmobiliariaflash.retrofit.generator.ServiceGenerator;
import com.salesianos.triana.inmobiliariaflash.retrofit.generator.TipoAutenticacion;
import com.salesianos.triana.inmobiliariaflash.retrofit.services.PropertyService;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PropertyMyFragment extends Fragment {

    // TODO: Customize parameter argument names
    private static final String ARG_COLUMN_COUNT = "column-count";
    // TODO: Customize parameters
    private int mColumnCount = 1;

    private Context ctx;

    public PropertyMyFragment() {
    }

    // TODO: Customize parameter initialization
    @SuppressWarnings("unused")
    public static PropertyMyFragment newInstance(int columnCount) {
        PropertyMyFragment fragment = new PropertyMyFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_COLUMN_COUNT, columnCount);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            mColumnCount = getArguments().getInt(ARG_COLUMN_COUNT);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_property_list, container, false);

        if (view instanceof RecyclerView) {
            Context context = view.getContext();
            final RecyclerView recyclerView = (RecyclerView) view;
            if (mColumnCount <= 1) {
                recyclerView.setLayoutManager(new LinearLayoutManager(context));
            } else {
                recyclerView.setLayoutManager(new GridLayoutManager(context, mColumnCount));
            }


            PropertyService service = ServiceGenerator.createService(PropertyService.class, UtilToken.getToken(this.ctx), TipoAutenticacion.JWT);
            Call<ResponseContainer<MyPropertyResponse>> call = service.getMyProperties();
            call.enqueue(new Callback<ResponseContainer<MyPropertyResponse>>() {
                @Override
                public void onResponse(Call<ResponseContainer<MyPropertyResponse>> call, Response<ResponseContainer<MyPropertyResponse>> response) {
                    if (response.isSuccessful()) {
                        recyclerView.setAdapter(new MyMyPropertyRecyclerViewAdapter(ctx, 3, response.body().getRows()));
                    } else {
                        Toast.makeText(ctx, "Error en la petición.", Toast.LENGTH_LONG).show();
                    }
                }

                @Override
                public void onFailure(Call<ResponseContainer<MyPropertyResponse>> call, Throwable t) {
                    Toast.makeText(ctx, "Error de conexión.", Toast.LENGTH_LONG).show();
                }
            });



        }
        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.ctx = context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }
}
