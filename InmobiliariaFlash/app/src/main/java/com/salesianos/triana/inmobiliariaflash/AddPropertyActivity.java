package com.salesianos.triana.inmobiliariaflash;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.salesianos.triana.inmobiliariaflash.model.CategoryResponse;
import com.salesianos.triana.inmobiliariaflash.model.CreatePropertyResponse;
import com.salesianos.triana.inmobiliariaflash.model.PhotoResponse;
import com.salesianos.triana.inmobiliariaflash.model.Property;
import com.salesianos.triana.inmobiliariaflash.model.ResponseContainer;
import com.salesianos.triana.inmobiliariaflash.model.UtilToken;
import com.salesianos.triana.inmobiliariaflash.retrofit.generator.ServiceGenerator;
import com.salesianos.triana.inmobiliariaflash.retrofit.generator.TipoAutenticacion;
import com.salesianos.triana.inmobiliariaflash.retrofit.services.CategoryService;
import com.salesianos.triana.inmobiliariaflash.retrofit.services.PhotoService;
import com.salesianos.triana.inmobiliariaflash.retrofit.services.PropertyService;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddPropertyActivity extends AppCompatActivity {

    private EditText etTitulo, etDesc, etPrecio, etRooms, etDirec, etCod, etCity, etProv, etLon, etLat, etSize;
    private Spinner spCategorias;
    private ImageView img;
    private Button btnUpload, btnAdd;

    private List<CategoryResponse> categorias;

    private static final int READ_REQUEST_CODE = 42;

    private Uri uriSelected;

    public static Context contextOfApplication;
    public static Context getContextOfApplication()
    {
        return contextOfApplication;
    }

    private MultipartBody.Part body;

    private String idCategorySelected;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_property);

        etTitulo = findViewById(R.id.et_titulo);
        etDesc = findViewById(R.id.et_desc);
        etPrecio = findViewById(R.id.et_precio);
        etRooms = findViewById(R.id.et_habitaciones);
        etDirec = findViewById(R.id.et_direccion);
        etCod = findViewById(R.id.et_cod_postal);
        etCity = findViewById(R.id.et_ciudad);
        etProv = findViewById(R.id.et_provincia);
        etLon = findViewById(R.id.et_lon);
        etLat = findViewById(R.id.et_lat);
        spCategorias = findViewById(R.id.sp_categorias);
        img = findViewById(R.id.ivToUpload);
        btnUpload = findViewById(R.id.btnUpload);
        btnAdd = findViewById(R.id.btn_add_property);
        etSize = findViewById(R.id.et_size);

        CategoryService service = ServiceGenerator.createService(CategoryService.class);
        Call <ResponseContainer<CategoryResponse>> call = service.getCategories();
        call.enqueue(new Callback<ResponseContainer<CategoryResponse>>() {
            @Override
            public void onResponse(Call<ResponseContainer<CategoryResponse>> calla, Response<ResponseContainer<CategoryResponse>> response) {
                if (response.isSuccessful()) {
                    categorias = response.body().getRows();
                    List<String> nombres = new ArrayList<>();
                    for (int i=0; i<categorias.size(); i++) {
                        nombres.add(categorias.get(i).getName());
                    }
                    ArrayAdapter<String> comboAdapter = new ArrayAdapter<String>(AddPropertyActivity.this,android.R.layout.simple_spinner_item, nombres);
                    spCategorias.setAdapter(comboAdapter);
                } else {
                    Toast.makeText(AddPropertyActivity.this, "Response False", Toast.LENGTH_LONG).show();
                }

            }

            @Override
            public void onFailure(Call<ResponseContainer<CategoryResponse>> calla, Throwable t) {
                // Toast
                Log.i("onFailure", "error en retrofit");
            }
        });

        btnUpload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                performFileSearch();
            }
        });

        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                for (int i=0; i<categorias.size(); i++) {
                    if (categorias.get(i).getName().equalsIgnoreCase(spCategorias.getSelectedItem().toString()))
                        idCategorySelected = categorias.get(i).getId();
                }
                PropertyService service = ServiceGenerator.createService(PropertyService.class,
                        UtilToken.getToken(AddPropertyActivity.this), TipoAutenticacion.JWT);
                Call<CreatePropertyResponse> call = service.addProperty(new Property(etTitulo.getText().toString(), etDesc.getText().toString(), Long.parseLong(etPrecio.getText().toString()), Integer.parseInt(etRooms.getText().toString()), Long.parseLong(etSize.getText().toString()), idCategorySelected, etDirec.getText().toString(), etCod.getText().toString(), etCity.getText().toString(), etProv.getText().toString(), etLon.getText().toString() + "," + etLat.getText().toString()));
                call.enqueue(new Callback<CreatePropertyResponse>() {
                    @Override
                    public void onResponse(Call<CreatePropertyResponse> call, Response<CreatePropertyResponse> response) {
                        if (response.isSuccessful()) {

                            if (uriSelected != null) {

                                try {
                                    InputStream inputStream = getContentResolver().openInputStream(uriSelected);
                                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                                    BufferedInputStream bufferedInputStream = new BufferedInputStream(inputStream);
                                    int cantBytes;
                                    byte[] buffer = new byte[1024*4];
                                    while ((cantBytes = bufferedInputStream.read(buffer,0,1024*4)) != -1) {
                                        baos.write(buffer,0,cantBytes);
                                    }
                                    RequestBody requestFile =
                                            RequestBody.create(
                                                    MediaType.parse(getContentResolver().getType(uriSelected)), baos.toByteArray());
                                    body = MultipartBody.Part.createFormData("photo", "photo", requestFile);
                                } catch (FileNotFoundException e) {
                                    e.printStackTrace();
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            }
                            RequestBody propertyId = RequestBody.create(MultipartBody.FORM, response.body().getId());
                            PhotoService service = ServiceGenerator.createService(PhotoService.class,
                                    UtilToken.getToken(AddPropertyActivity.this), TipoAutenticacion.JWT);
                            Call<PhotoResponse> call1 = service.addPhoto(body, propertyId);
                            call1.enqueue(new Callback<PhotoResponse>() {
                                @Override
                                public void onResponse(Call<PhotoResponse> calla, Response<PhotoResponse> response) {
                                    if (response.isSuccessful()) {
                                        startActivity(new Intent(AddPropertyActivity.this, HomeActivity.class));
                                        finish();
                                    } else {
                                        Toast.makeText(AddPropertyActivity.this, "Response False", Toast.LENGTH_LONG).show();
                                    }
                                }
                                @Override
                                public void onFailure(Call<PhotoResponse> calla, Throwable t) {
                                    // Toast
                                    Log.i("onFailure", "error en retrofit");
                                }
                            });

                        } else {
                            Toast.makeText(AddPropertyActivity.this, "Response False", Toast.LENGTH_LONG).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<CreatePropertyResponse> call, Throwable t) {
                        Toast.makeText(AddPropertyActivity.this, "Error de conexion", Toast.LENGTH_LONG).show();
                    }
                });
            }
        });

    }

    public void performFileSearch(){
        Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        intent.setType("image/*");
        startActivityForResult(intent, READ_REQUEST_CODE);
    }

    public void onActivityResult( int requestCode, int resultCode,
                                  Intent resultData) {
        if (requestCode == READ_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            Uri uri = null;
            if (resultData != null) {
                uri = resultData.getData();
                Log.i("Filechooser URI", "Uri: " + uri.toString());
                Glide
                        .with(this)
                        .load(uri)
                        .into(img);
                uriSelected = uri;
            }
        }
    }
}
