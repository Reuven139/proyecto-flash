package com.salesianos.triana.inmobiliariaflash;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.salesianos.triana.inmobiliariaflash.model.CategoryResponse;
import com.salesianos.triana.inmobiliariaflash.model.PhotoResponse;
import com.salesianos.triana.inmobiliariaflash.model.PropertyOneResponse;
import com.salesianos.triana.inmobiliariaflash.model.PropertyResponse;
import com.salesianos.triana.inmobiliariaflash.model.ResponseContainer;
import com.salesianos.triana.inmobiliariaflash.model.UserResponseProperty;
import com.salesianos.triana.inmobiliariaflash.model.UtilToken;
import com.salesianos.triana.inmobiliariaflash.retrofit.generator.ServiceGenerator;
import com.salesianos.triana.inmobiliariaflash.retrofit.generator.TipoAutenticacion;
import com.salesianos.triana.inmobiliariaflash.retrofit.services.PhotoService;
import com.salesianos.triana.inmobiliariaflash.retrofit.services.PropertyService;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PropertyDetailFragment extends Fragment {

    private Context ctx;

    public static final String ARG_ITEM_ID = "item_id";
    public static final String ARG_ITEM_MINE = "item_mine";

    private PropertyResponse mItem;

    private int count=0;

    private List<PhotoResponse> listPhotos;
    private PhotoResponse selected;

    public PropertyDetailFragment() { }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.fragment_property_detail, container, false);

        if (getArguments().containsKey(ARG_ITEM_ID)) {

            final String idProperty = getArguments().getString(ARG_ITEM_ID);
            final String mine = getArguments().getString(ARG_ITEM_MINE);

            PropertyService service = ServiceGenerator.createService(PropertyService.class);
            Call<PropertyOneResponse> call = service.getProperty(idProperty);
            call.enqueue(new Callback<PropertyOneResponse>() {
                @Override
                public void onResponse(Call<PropertyOneResponse> call, final Response<PropertyOneResponse> response) {
                    if (response.isSuccessful()) {

                        mItem = new PropertyResponse(
                                response.body().getProperty().getTitle(),
                                response.body().getProperty().getDescription(),
                                response.body().getProperty().getPrice(),
                                response.body().getProperty().getRooms(),
                                response.body().getProperty().getSize(),
                                response.body().getProperty().getCategoryId(),
                                response.body().getProperty().getAddress(),
                                response.body().getProperty().getZipcode(),
                                response.body().getProperty().getCity(),
                                response.body().getProperty().getProvince(),
                                response.body().getProperty().getLoc(),
                                response.body().getProperty().getOwnerId(),
                                response.body().getProperty().getCreatedAt(),
                                response.body().getProperty().getUpdatedAt(),
                                response.body().getProperty().getId(),
                                response.body().getProperty().getPhotos());
                        ((TextView) rootView.findViewById(R.id.titulo)).setText(mItem.getTitle());
                        ((TextView) rootView.findViewById(R.id.desc)).setText(mItem.getDescription());
                        ((TextView) rootView.findViewById(R.id.rooms)).setText("Habitaciones: " + mItem.getRooms().toString());
                        ((TextView) rootView.findViewById(R.id.price)).setText(mItem.getPrice().toString() + "€");
                        ((TextView) rootView.findViewById(R.id.size)).setText("Tamaño: " + mItem.getSize().toString() + "m2");
                        ((TextView) rootView.findViewById(R.id.direc)).setText(mItem.getAddress());
                        ((TextView) rootView.findViewById(R.id.direc2)).setText(mItem.getZipcode() + ", " + mItem.getCity() + ", " + mItem.getProvince());

                        PhotoService service = ServiceGenerator.createService(PhotoService.class);
                        Call<ResponseContainer<PhotoResponse>> callPhotos = service.getPhotos();
                        callPhotos.enqueue(new Callback<ResponseContainer<PhotoResponse>>() {
                            @Override
                            public void onResponse(Call<ResponseContainer<PhotoResponse>> calla, Response<ResponseContainer<PhotoResponse>> response) {
                                if (response.isSuccessful()) {
                                    if (response.body().getRows() != null)
                                        listPhotos = response.body().getRows();
                                } else {
                                    Toast.makeText(ctx, "Response False", Toast.LENGTH_LONG).show();
                                }
                            }
                            @Override
                            public void onFailure(Call<ResponseContainer<PhotoResponse>> calla, Throwable t) {
                                // Toast
                                Log.i("onFailure", "error en retrofit");
                            }
                        });

                        if (getArguments().getString(ARG_ITEM_MINE).equalsIgnoreCase("y")) {

                            ((ImageView) rootView.findViewById(R.id.photo)).setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    AlertDialog.Builder builder = new AlertDialog.Builder(ctx);
                                    builder.setMessage("¿Desea borrar esta imagen?")
                                            .setTitle("Eliminar imagen");
                                    builder.setPositiveButton("Sí", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            for (PhotoResponse phRes : listPhotos) {
                                                if (mItem.getPhotos().get(count).equalsIgnoreCase(phRes.getImgurLink()))
                                                    selected = phRes;
                                            }
                                            PhotoService service = ServiceGenerator.createService(PhotoService.class,
                                                    UtilToken.getToken(ctx), TipoAutenticacion.JWT);
                                            Call callDeletePhoto = service.deletePhoto(selected.getId());
                                            callDeletePhoto.enqueue(new Callback() {
                                                @Override
                                                public void onResponse(Call calla, Response response) {
                                                    if (response.isSuccessful()) {
                                                        Toast.makeText(ctx, "Borrado exitosamente", Toast.LENGTH_LONG).show();
                                                    } else {
                                                        Toast.makeText(ctx, "Response False", Toast.LENGTH_LONG).show();
                                                    }
                                                }

                                                @Override
                                                public void onFailure(Call calla, Throwable t) {
                                                    // Toast
                                                    Log.i("onFailure", "error en retrofit");
                                                }
                                            });
                                        }
                                    });
                                    builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            dialog.dismiss();
                                        }
                                    });
                                    AlertDialog dialog = builder.create();
                                    dialog.show();
                                }
                            });
                        }

                        if (mItem.getPhotos().size()!=0)
                            Glide
                                    .with(ctx)
                                    .load(mItem.getPhotos().get(0))
                                    .apply(new RequestOptions()
                                            .diskCacheStrategy(DiskCacheStrategy.NONE)
                                            .skipMemoryCache(true))
                                    .into((ImageView) rootView.findViewById(R.id.photo));
                        else
                            Glide
                                    .with(ctx)
                                    .load("https://www.riverside.org.uk/wp-content/themes/rd-riverside/grunticon/png/no-images-placeholder.png")
                                    .into((ImageView) rootView.findViewById(R.id.photo));

                        if (mItem.getPhotos().size()==0){
                            ((ImageView) rootView.findViewById(R.id.imageViewLeftArrow)).setEnabled(false);
                            ((ImageView) rootView.findViewById(R.id.imageViewRightArrow)).setEnabled(false);
                        } else {
                            ((ImageView) rootView.findViewById(R.id.imageViewLeftArrow)).setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    count++;
                                    if (count>mItem.getPhotos().size()-1){
                                        count=0;
                                    }
                                    Glide
                                            .with(ctx)
                                            .load(mItem.getPhotos().get(count))
                                            .apply(new RequestOptions()
                                                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                                                    .skipMemoryCache(true))
                                            .into((ImageView) rootView.findViewById(R.id.photo));
                                }
                            });
                            ((ImageView) rootView.findViewById(R.id.imageViewRightArrow)).setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    count--;
                                    if (count<0){
                                        count=mItem.getPhotos().size()-1;
                                    }
                                    Glide
                                            .with(ctx)
                                            .load(mItem.getPhotos().get(count))
                                            .apply(new RequestOptions()
                                                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                                                    .skipMemoryCache(true))
                                            .into((ImageView) rootView.findViewById(R.id.photo));
                                }
                            });


                        }
                        
                    } else {
                        Toast.makeText(ctx, "Error en la petición.", Toast.LENGTH_LONG).show();
                    }
                }

                @Override
                public void onFailure(Call<PropertyOneResponse> call, Throwable t) {
                    Toast.makeText(ctx, "Error de conexión.", Toast.LENGTH_LONG).show();
                }
            });
        }

        return rootView;

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.ctx = context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }
}
