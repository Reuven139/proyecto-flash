package com.salesianos.triana.inmobiliariaflash.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class PropertyOneResponse {

    private PropertyResponse rows;
    private long count;

    public PropertyOneResponse() { }

    public PropertyOneResponse(PropertyResponse rows, long count) {
        this.rows = rows;
        this.count = count;
    }

    public PropertyResponse getProperty() {
        return rows;
    }

    public void setProperty(PropertyResponse property) {
        this.rows = property;
    }

    public long getCount() {
        return count;
    }

    public void setCount(long count) {
        this.count = count;
    }

}
