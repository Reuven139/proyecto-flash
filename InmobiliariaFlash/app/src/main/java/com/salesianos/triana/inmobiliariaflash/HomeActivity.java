package com.salesianos.triana.inmobiliariaflash;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.salesianos.triana.inmobiliariaflash.model.UtilToken;

public class HomeActivity extends AppCompatActivity {

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            Fragment f = null;
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    f = new PropertyFragment();
                    break;
                case R.id.navigation_dashboard:
                    if (UtilToken.getToken(HomeActivity.this) != null)
                        f = new PropertyFavFragment();
                    break;
                case R.id.navigation_notifications:
                    if (UtilToken.getToken(HomeActivity.this) != null)
                        f = new PropertyMyFragment();
                    break;
            }
            if(f != null) {
                getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.frame, f)
                        .commit();
            } else {
                startActivity(new Intent(HomeActivity.this, LoginActivity.class));
                finish();
            }
            return true;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.frame, new PropertyFragment())
                .commit();

        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        menu.findItem(R.id.action_login).setVisible(true);
        menu.findItem(R.id.action_logout).setVisible(true);
        menu.findItem(R.id.action_add_property).setVisible(true);
        if (UtilToken.getToken(HomeActivity.this) == null) {
            menu.findItem(R.id.action_logout).setVisible(false);
            menu.findItem(R.id.action_add_property).setVisible(false);
        } else {
            menu.findItem(R.id.action_login).setVisible(false);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id) {
            case R.id.action_login:
                startActivity(new Intent(this, LoginActivity.class));
                finish();
                return true;
            case R.id.action_logout:
                UtilToken.setIdUser(this, null);
                UtilToken.setToken(this, null);
                startActivity(new Intent(this, HomeActivity.class));
                finish();
                return true;
            case R.id.action_add_property:
                startActivity(new Intent(this, AddPropertyActivity.class));
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {

            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("¿Quiere cerrar la aplicación?")
                    .setTitle("Cerrar");
            builder.setPositiveButton("Sí", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    UtilToken.setIdUser(HomeActivity.this, null);
                    UtilToken.setToken(HomeActivity.this, null);
                    finish();
                }
            });
            builder.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    dialog.dismiss();
                }
            });
            AlertDialog dialog = builder.create();
            dialog.show();
    }
}
