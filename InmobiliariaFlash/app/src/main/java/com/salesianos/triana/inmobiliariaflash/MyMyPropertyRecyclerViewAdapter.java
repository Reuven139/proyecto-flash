package com.salesianos.triana.inmobiliariaflash;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.salesianos.triana.inmobiliariaflash.model.MyPropertyResponse;
import com.salesianos.triana.inmobiliariaflash.model.PropertyResponse;
import com.salesianos.triana.inmobiliariaflash.model.ResponseContainer;
import com.salesianos.triana.inmobiliariaflash.model.User;
import com.salesianos.triana.inmobiliariaflash.model.UtilToken;
import com.salesianos.triana.inmobiliariaflash.retrofit.generator.ServiceGenerator;
import com.salesianos.triana.inmobiliariaflash.retrofit.generator.TipoAutenticacion;
import com.salesianos.triana.inmobiliariaflash.retrofit.services.PropertyService;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MyMyPropertyRecyclerViewAdapter extends RecyclerView.Adapter<MyMyPropertyRecyclerViewAdapter.ViewHolder> {

    private List<MyPropertyResponse> mValues;
    private Context ctx;
    private int fragment;

    public MyMyPropertyRecyclerViewAdapter(Context ctx, int fragment, List<MyPropertyResponse> items) {
        mValues = items;
        this.ctx = ctx;
        this.fragment = fragment;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_property, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        holder.mItem = mValues.get(position);
        holder.mTitulo.setText(mValues.get(position).getTitle());
        holder.mDireccion.setText(mValues.get(position).getAddress());
        if (mValues.get(position).getPhotos() != null)
            Glide
                    .with(ctx)
                    .load(mValues.get(position).getPhotos().get(0))
                    .centerCrop()
                    .into(holder.mImagen);
        else

            Glide
                    .with(ctx)
                    .load("https://andaluciainformacion.es/media/503646/donde-encontrar-anuncios-de-pisos-en-sevilla.jpg")
                    .centerCrop()
                    .into(holder.mImagen);

        holder.itemView.setTag(mValues.get(position));
        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyPropertyResponse item = (MyPropertyResponse) v.getTag();
                Intent intent = new Intent(ctx, MyPropertyDetailActivity.class);
                intent.putExtra(PropertyDetailFragment.ARG_ITEM_ID, item.getId());
                intent.putExtra(PropertyDetailFragment.ARG_ITEM_MINE, "y");
                ctx.startActivity(intent);
            }
        });

        holder.mFav.setVisibility(View.INVISIBLE);

    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView mTitulo;
        public final TextView mDireccion;
        public final ImageView mImagen;
        public final ImageView mFav;
        public MyPropertyResponse mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mTitulo = view.findViewById(R.id.tv_titulo);
            mDireccion = view.findViewById(R.id.tv_direccion);
            mImagen = view.findViewById(R.id.iv_imagen);
            mFav = view.findViewById(R.id.iv_fav);
        }

    }

    public void setData(List<MyPropertyResponse> mValues) {
        this.mValues = mValues;
        notifyDataSetChanged();
    }
}
