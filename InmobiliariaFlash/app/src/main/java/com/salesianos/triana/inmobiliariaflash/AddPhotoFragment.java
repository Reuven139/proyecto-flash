package com.salesianos.triana.inmobiliariaflash;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.salesianos.triana.inmobiliariaflash.model.PhotoResponse;
import com.salesianos.triana.inmobiliariaflash.model.UtilToken;
import com.salesianos.triana.inmobiliariaflash.retrofit.generator.ServiceGenerator;
import com.salesianos.triana.inmobiliariaflash.retrofit.generator.TipoAutenticacion;
import com.salesianos.triana.inmobiliariaflash.retrofit.services.PhotoService;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddPhotoFragment extends DialogFragment {

    public static final String ARG_ITEM_ID = "idProperty";
    private String idPro;
    private ImageView ivToUpload;
    private Button btnUpload;
    private Context ctx;

    private static final int READ_REQUEST_CODE = 42;
    private Uri uriSelected;
    private MultipartBody.Part body;

    public AddPhotoFragment() {
        // Required empty public constructor
    }

    public static AddPhotoFragment newInstance(String idProperty) {
        AddPhotoFragment fragment = new AddPhotoFragment();
        Bundle args = new Bundle();
        args.putString(ARG_ITEM_ID, idProperty);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            idPro = getArguments().getString(ARG_ITEM_ID);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_add_photo, container, false);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View v = inflater.inflate(R.layout.fragment_add_photo, null);
        ivToUpload = v.findViewById(R.id.ivToUpload);
        btnUpload = v.findViewById(R.id.btnUpload2);
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Añadir imagen");
        builder.setMessage("Cargue su imagen")
                .setPositiveButton("Añadir", new DialogInterface.OnClickListener() {



                    public void onClick(DialogInterface dialog, int id) {
                        if (uriSelected != null) {
                            try {
                                InputStream inputStream = getActivity().getContentResolver().openInputStream(uriSelected);
                                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                                BufferedInputStream bufferedInputStream = new BufferedInputStream(inputStream);
                                int cantBytes;
                                byte[] buffer = new byte[1024*4];
                                while ((cantBytes = bufferedInputStream.read(buffer,0,1024*4)) != -1) {
                                    baos.write(buffer,0,cantBytes);
                                }
                                RequestBody requestFile =
                                        RequestBody.create(
                                                MediaType.parse(getActivity().getContentResolver().getType(uriSelected)), baos.toByteArray());
                                body = MultipartBody.Part.createFormData("photo", "photo", requestFile);
                            } catch (FileNotFoundException e) {
                                e.printStackTrace();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                        RequestBody propertyId = RequestBody.create(MultipartBody.FORM, idPro);
                        PhotoService service = ServiceGenerator.createService(PhotoService.class,
                                UtilToken.getToken(ctx), TipoAutenticacion.JWT);
                        Call<PhotoResponse> call = service.addPhoto(body, propertyId);
                        call.enqueue(new Callback<PhotoResponse>() {
                            @Override
                            public void onResponse(Call<PhotoResponse> calla, Response<PhotoResponse> response) {
                                if (response.isSuccessful()) {
                                    Toast.makeText(ctx, "Añadida correctamente.", Toast.LENGTH_LONG).show();
                                } else {
                                    Toast.makeText(ctx, "Response False", Toast.LENGTH_LONG).show();
                                }
                            }
                            @Override
                            public void onFailure(Call<PhotoResponse> calla, Throwable t) {
                                // Toast
                                Log.i("onFailure", "error en retrofit");
                            }
                        });
                    }
                });

        btnUpload = v.findViewById(R.id.btnUpload2);
        btnUpload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                performFileSearch();
            }
        });
        builder.setView(v);
        return builder.create();
    }

    public void performFileSearch(){
        Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        intent.setType("image/*");
        startActivityForResult(intent, READ_REQUEST_CODE);
    }

    public void onActivityResult( int requestCode, int resultCode,
                                  Intent resultData) {
        if (requestCode == READ_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            Uri uri = null;
            if (resultData != null) {
                uri = resultData.getData();
                Log.i("Filechooser URI", "Uri: " + uri.toString());
                Glide
                        .with(this)
                        .load(uri)
                        .into(ivToUpload);
                uriSelected = uri;
            }
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.ctx = context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }
}
