package com.salesianos.triana.inmobiliariaflash.retrofit.services;

import com.salesianos.triana.inmobiliariaflash.model.CategoryResponse;
import com.salesianos.triana.inmobiliariaflash.model.ResponseContainer;

import retrofit2.Call;
import retrofit2.http.GET;

public interface CategoryService {

    @GET("categories")
    Call<ResponseContainer<CategoryResponse>> getCategories();

}
