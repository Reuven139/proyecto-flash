package com.salesianos.triana.inmobiliariaflash.retrofit.services;

import com.salesianos.triana.inmobiliariaflash.model.PhotoResponse;
import com.salesianos.triana.inmobiliariaflash.model.ResponseContainer;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;

public interface PhotoService {

    @Multipart
    @POST("photos")
    Call<PhotoResponse> addPhoto(@Part MultipartBody.Part photo, @Part("propertyId") RequestBody propertyId);

    @DELETE("photos/{id}")
    Call<ResponseBody> deletePhoto(@Path("id") String id);

    @GET("photos")
    Call<ResponseContainer<PhotoResponse>> getPhotos();

}
