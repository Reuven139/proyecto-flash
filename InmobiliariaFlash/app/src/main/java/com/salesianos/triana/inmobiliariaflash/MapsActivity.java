package com.salesianos.triana.inmobiliariaflash;

import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.salesianos.triana.inmobiliariaflash.model.PropertyOneResponse;
import com.salesianos.triana.inmobiliariaflash.model.PropertyResponse;
import com.salesianos.triana.inmobiliariaflash.retrofit.generator.ServiceGenerator;
import com.salesianos.triana.inmobiliariaflash.retrofit.services.PropertyService;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setMinZoomPreference(13);
        mMap.setMaxZoomPreference(18);

        String id = getIntent().getStringExtra("id");

        PropertyService service = ServiceGenerator.createService(PropertyService.class);
        Call<PropertyOneResponse> call = service.getProperty(id);
        call.enqueue(new Callback<PropertyOneResponse>() {
            @Override
            public void onResponse(Call<PropertyOneResponse> call, Response<PropertyOneResponse> response) {
                if (response.isSuccessful()) {
                    PropertyResponse property = response.body().getProperty();
                    String loc = property.getLoc();
                    String latLng[] = loc.split(",");
                    LatLng propertyLoc = new LatLng(Float.parseFloat(latLng[0]), Float.parseFloat(latLng[1]));
                    mMap.addMarker(new MarkerOptions().position(propertyLoc).title(property.getTitle()));
                    mMap.animateCamera(CameraUpdateFactory.newLatLng(propertyLoc));
                } else {
                    Toast.makeText(MapsActivity.this, "Error en la petición.", Toast.LENGTH_LONG).show();
                }
            }
            @Override
            public void onFailure(Call<PropertyOneResponse> call, Throwable t) {
                Toast.makeText(MapsActivity.this, "Error de conexión.", Toast.LENGTH_LONG).show();
            }
        });
    }
}
