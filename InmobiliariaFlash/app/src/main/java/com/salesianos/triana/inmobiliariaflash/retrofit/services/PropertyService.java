package com.salesianos.triana.inmobiliariaflash.retrofit.services;

import com.salesianos.triana.inmobiliariaflash.model.CreatePropertyResponse;
import com.salesianos.triana.inmobiliariaflash.model.LoginResponse;
import com.salesianos.triana.inmobiliariaflash.model.MyPropertyResponse;
import com.salesianos.triana.inmobiliariaflash.model.Property;
import com.salesianos.triana.inmobiliariaflash.model.PropertyOneResponse;
import com.salesianos.triana.inmobiliariaflash.model.PropertyResponse;
import com.salesianos.triana.inmobiliariaflash.model.ResponseContainer;
import com.salesianos.triana.inmobiliariaflash.model.User;

import okhttp3.Response;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface PropertyService {

    @GET("properties")
    Call<ResponseContainer<PropertyResponse>> getProperties();

    @GET("properties/auth")
    Call<ResponseContainer<PropertyResponse>> getPropertiesLogged();

    @GET("properties/{id}")
    Call<PropertyOneResponse> getProperty(@Path("id") String id);

    @POST("properties/fav/{id}")
    Call<User> favProperty(@Path("id") String id);

    @DELETE("properties/fav/{id}")
    Call<ResponseBody> noFavProperty(@Path("id") String id);

    @GET("properties/fav")
    Call<ResponseContainer<PropertyResponse>> getFavProperties();

    @GET("properties/mine")
    Call<ResponseContainer<MyPropertyResponse>> getMyProperties();

    @POST("properties")
    Call<CreatePropertyResponse> addProperty(@Body Property property);

    @DELETE("properties/{id}")
    Call<ResponseBody> deleteProperty(@Path("id") String id);

}
