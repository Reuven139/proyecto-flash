package com.salesianos.triana.inmobiliariaflash.retrofit.generator;

public enum TipoAutenticacion {
    SIN_AUTENTICACION, BASIC, JWT
}
