package com.salesianos.triana.inmobiliariaflash;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.NavUtils;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.salesianos.triana.inmobiliariaflash.model.PhotoResponse;
import com.salesianos.triana.inmobiliariaflash.model.UtilToken;
import com.salesianos.triana.inmobiliariaflash.retrofit.generator.ServiceGenerator;
import com.salesianos.triana.inmobiliariaflash.retrofit.generator.TipoAutenticacion;
import com.salesianos.triana.inmobiliariaflash.retrofit.services.PhotoService;
import com.salesianos.triana.inmobiliariaflash.retrofit.services.PropertyService;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MyPropertyDetailActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_property_detail);
        getSupportActionBar().hide();

        FloatingActionButton fab2 = (FloatingActionButton) findViewById(R.id.fabAdd);
        fab2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mostrarDialogAddPhoto(getIntent().getStringExtra(PropertyDetailFragment.ARG_ITEM_ID));
            }
        });

        FloatingActionButton fab3 = findViewById(R.id.fabEdit);
        fab3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(MyPropertyDetailActivity.this, "No está añadida la funcionalidad de editar propiedad.", Toast.LENGTH_LONG).show();
            }
        });

        FloatingActionButton fab4 = (FloatingActionButton) findViewById(R.id.fabDelete);
        fab4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(MyPropertyDetailActivity.this);
                builder.setMessage("¿De verdad quiere borrar la propiedad?")
                        .setTitle("Eliminar propiedad");
                builder.setPositiveButton("Sí", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        PropertyService service = ServiceGenerator.createService(PropertyService.class,
                                UtilToken.getToken(MyPropertyDetailActivity.this), TipoAutenticacion.JWT);
                        Call call = service.deleteProperty(getIntent().getStringExtra(PropertyDetailFragment.ARG_ITEM_ID));
                        call.enqueue(new Callback() {
                            @Override
                            public void onResponse(Call calla, Response response) {
                                if (response.isSuccessful()) {
                                    Toast.makeText(MyPropertyDetailActivity.this, "Borrada correctamente.", Toast.LENGTH_LONG).show();
                                } else {
                                    Toast.makeText(MyPropertyDetailActivity.this, "Response False", Toast.LENGTH_LONG).show();
                                }
                            }
                            @Override
                            public void onFailure(Call calla, Throwable t) {
                                // Toast
                                Log.i("onFailure", "error en retrofit");
                            }
                        });
                        finish();
                    }
                });
                builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                });
                AlertDialog dialog = builder.create();
                dialog.show();
            }
        });

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(MyPropertyDetailActivity.this, MapsActivity.class);
                String id = getIntent().getStringExtra(PropertyDetailFragment.ARG_ITEM_ID);
                i.putExtra("id", id);
                startActivity(i);
            }
        });

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        if (savedInstanceState == null) {
            Bundle arguments = new Bundle();
            arguments.putString(PropertyDetailFragment.ARG_ITEM_ID,
                    getIntent().getStringExtra(PropertyDetailFragment.ARG_ITEM_ID));
            arguments.putString(PropertyDetailFragment.ARG_ITEM_MINE,
                    getIntent().getStringExtra(PropertyDetailFragment.ARG_ITEM_MINE));
            PropertyDetailFragment fragment = new PropertyDetailFragment();
            fragment.setArguments(arguments);
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.item_detail_container, fragment)
                    .commit();
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            NavUtils.navigateUpTo(this, new Intent(this, HomeActivity.class));
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void mostrarDialogAddPhoto(String idProperty){
        DialogFragment dialog = AddPhotoFragment.newInstance(idProperty);
        dialog.show(getSupportFragmentManager(), "AddPhotoFragment");
    }
}
