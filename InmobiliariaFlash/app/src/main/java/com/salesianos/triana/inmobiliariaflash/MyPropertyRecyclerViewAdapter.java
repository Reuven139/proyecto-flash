package com.salesianos.triana.inmobiliariaflash;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.salesianos.triana.inmobiliariaflash.model.PropertyResponse;
import com.salesianos.triana.inmobiliariaflash.model.ResponseContainer;
import com.salesianos.triana.inmobiliariaflash.model.User;
import com.salesianos.triana.inmobiliariaflash.model.UtilToken;
import com.salesianos.triana.inmobiliariaflash.retrofit.generator.ServiceGenerator;
import com.salesianos.triana.inmobiliariaflash.retrofit.generator.TipoAutenticacion;
import com.salesianos.triana.inmobiliariaflash.retrofit.services.PropertyService;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MyPropertyRecyclerViewAdapter extends RecyclerView.Adapter<MyPropertyRecyclerViewAdapter.ViewHolder> {

    private List<PropertyResponse> mValues;
    private Context ctx;
    private int fragment;

    public MyPropertyRecyclerViewAdapter(Context ctx, int fragment, List<PropertyResponse> items) {
        mValues = items;
        this.ctx = ctx;
        this.fragment = fragment;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_property, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        holder.mItem = mValues.get(position);
        holder.mTitulo.setText(mValues.get(position).getTitle());
        holder.mDireccion.setText(mValues.get(position).getAddress());
        if (mValues.get(position).getPhotos() != null)
            Glide
                    .with(ctx)
                    .load(mValues.get(position).getPhotos().get(0))
                    .centerCrop()
                    .into(holder.mImagen);
        else

            Glide
                    .with(ctx)
                    .load("https://andaluciainformacion.es/media/503646/donde-encontrar-anuncios-de-pisos-en-sevilla.jpg")
                    .centerCrop()
                    .into(holder.mImagen);

        holder.itemView.setTag(mValues.get(position));
        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PropertyResponse item = (PropertyResponse) v.getTag();
                Intent intent = new Intent(ctx, PropertyDetailActivity.class);
                intent.putExtra(PropertyDetailFragment.ARG_ITEM_ID, item.getId());
                intent.putExtra(PropertyDetailFragment.ARG_ITEM_MINE, "n");
                ctx.startActivity(intent);
            }
        });

        if (UtilToken.getToken(ctx) == null || fragment == 2 || fragment == 3) {
            holder.mFav.setVisibility(View.INVISIBLE);
        } else {
            holder.mFav.setVisibility(View.VISIBLE);
            if(mValues.get(position).isFav()) {
                holder.mFav.setImageResource(R.drawable.ic_star_red_24dp);
                holder.mFav.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        PropertyService service = ServiceGenerator.createService(PropertyService.class, UtilToken.getToken(ctx), TipoAutenticacion.JWT);
                        Call call = service.noFavProperty(mValues.get(position).getId());
                        call.enqueue(new Callback() {
                            @Override
                            public void onResponse(Call call, Response response) {
                                if (response.isSuccessful()) {
                                    PropertyService service = ServiceGenerator.createService(PropertyService.class, UtilToken.getToken(ctx), TipoAutenticacion.JWT);
                                    call = service.getPropertiesLogged();
                                    call.enqueue(new Callback<ResponseContainer<PropertyResponse>>() {
                                        @Override
                                        public void onResponse(Call<ResponseContainer<PropertyResponse>> call, Response<ResponseContainer<PropertyResponse>> response) {
                                            if (response.isSuccessful()) {
                                                setData(response.body().getRows());
                                            } else {
                                                Toast.makeText(ctx, "Error en la petición.", Toast.LENGTH_LONG).show();
                                            }
                                        }

                                        @Override
                                        public void onFailure(Call<ResponseContainer<PropertyResponse>> call, Throwable t) {
                                            Toast.makeText(ctx, "Error de conexión.", Toast.LENGTH_LONG).show();
                                        }
                                    });
                                } else {
                                    Toast.makeText(ctx, "Error en la petición.", Toast.LENGTH_LONG).show();
                                }
                            }

                            @Override
                            public void onFailure(Call call, Throwable t) {
                                Toast.makeText(ctx, "Error de conexión.", Toast.LENGTH_LONG).show();
                            }
                        });
                    }
                });
            } else {
                holder.mFav.setImageResource(R.drawable.ic_star_border_void_24dp);
                holder.mFav.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        PropertyService service = ServiceGenerator.createService(PropertyService.class, UtilToken.getToken(ctx), TipoAutenticacion.JWT);
                        Call<User> call = service.favProperty(mValues.get(position).getId());
                        call.enqueue(new Callback<User>() {
                            @Override
                            public void onResponse(Call<User> call, Response<User> response) {
                                if (response.isSuccessful()) {
                                    PropertyService service = ServiceGenerator.createService(PropertyService.class, UtilToken.getToken(ctx), TipoAutenticacion.JWT);
                                    Call<ResponseContainer<PropertyResponse>> call1 = service.getPropertiesLogged();
                                    call1.enqueue(new Callback<ResponseContainer<PropertyResponse>>() {
                                        @Override
                                        public void onResponse(Call<ResponseContainer<PropertyResponse>> call, Response<ResponseContainer<PropertyResponse>> response) {
                                            if (response.isSuccessful()) {
                                                setData(response.body().getRows());
                                            } else {
                                                Toast.makeText(ctx, "Error en la petición.", Toast.LENGTH_LONG).show();
                                            }
                                        }

                                        @Override
                                        public void onFailure(Call<ResponseContainer<PropertyResponse>> call, Throwable t) {
                                            Toast.makeText(ctx, "Error de conexión.", Toast.LENGTH_LONG).show();
                                        }
                                    });
                                } else {
                                    Toast.makeText(ctx, "Error en la petición.", Toast.LENGTH_LONG).show();
                                }
                            }

                            @Override
                            public void onFailure(Call<User> call, Throwable t) {
                                Toast.makeText(ctx, "Error de conexión.", Toast.LENGTH_LONG).show();
                            }
                        });
                    }
                });
            }


        }

    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView mTitulo;
        public final TextView mDireccion;
        public final ImageView mImagen;
        public final ImageView mFav;
        public PropertyResponse mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mTitulo = view.findViewById(R.id.tv_titulo);
            mDireccion = view.findViewById(R.id.tv_direccion);
            mImagen = view.findViewById(R.id.iv_imagen);
            mFav = view.findViewById(R.id.iv_fav);
        }

    }

    public void setData(List<PropertyResponse> mValues) {
        this.mValues = mValues;
        notifyDataSetChanged();
    }
}
